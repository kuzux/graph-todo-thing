package db

import (
	"database/sql"

	_ "github.com/tursodatabase/go-libsql"
)

type Item struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Parent   string `json:"parent"`
	Done     bool   `json:"done"`
	Deadline string `json:"deadline"`
}

func Connect() (*sql.DB, error) {
	// TODO: Connection pooling
	conn, err := sql.Open("libsql", "file:items.db")
	if err != nil {
		return nil, err
	}
	err = initSchema(conn)
	if err != nil {
		return nil, err
	}
	return conn, nil
}

func initSchema(db *sql.DB) error {
	_, err := db.Exec("CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY, uuid TEXT, name TEXT, parent TEXT, done BOOLEAN, deadline TEXT)")
	return err
}

func GetItems(db *sql.DB) ([]Item, error) {
	rows, err := db.Query("SELECT uuid, name, parent, done, deadline FROM items")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var items []Item
	for rows.Next() {
		var item Item
		err := rows.Scan(&item.ID, &item.Name, &item.Parent, &item.Done, &item.Deadline)
		if err != nil {
			return nil, err
		}
		items = append(items, item)
	}

	return items, nil
}

func AddItem(db *sql.DB, item Item) error {
	_, err := db.Exec("INSERT INTO items (uuid, name, parent, done, deadline) VALUES (?, ?, ?, ?, ?)", item.ID, item.Name, item.Parent, item.Done, item.Deadline)
	return err
}

func UpdateItem(db *sql.DB, item Item) error {
	_, err := db.Exec("UPDATE items SET name = ?, parent = ?, done = ?, deadline = ? WHERE uuid = ?", item.Name, item.Parent, item.Done, item.Deadline, item.ID)
	return err
}

func DeleteItem(db *sql.DB, id string) error {
	_, err := db.Exec("DELETE FROM items WHERE uuid = ?", id)
	return err
}
