package main

import (
	"fmt"
	"net/http"

	"todograph.thing/backend/endpoints"
)

func requestLogger(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("%s %s %s\n", r.RemoteAddr, r.Method, r.URL)
		h.ServeHTTP(w, r)
	})
}

func allowCors(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		if r.Method != "OPTIONS" {
			h.ServeHTTP(w, r)
		} else {
			w.WriteHeader(http.StatusOK)
		}
	})
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/health-check", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Healthy")
	})
	mux.HandleFunc("/items", endpoints.GetItems)
	mux.HandleFunc("/add-item", endpoints.AddItem)
	mux.HandleFunc("/update-item", endpoints.UpdateItem)
	mux.HandleFunc("/delete-item", endpoints.DeleteItem)

	fmt.Println("Serving on port 3579...")
	http.ListenAndServe(":3579", requestLogger(allowCors(mux)))
}
