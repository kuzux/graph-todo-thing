import styles from './style.module.css'
import Graph from "graphology";
import ForceSupervisor from "graphology-layout-force/worker";
import { EdgeArrowProgram } from 'sigma/rendering';
import { createNodeBorderProgram } from '@sigma/node-border';
import Sigma from "sigma";

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
    <div class=${styles.container}>
        <div class=${styles.display}></div>
    </div>
    <div class=${styles.editor}>
        <input type="text" name="label" placeholder="Label" />
        <p>
            <input type="checkbox" name="done" />
            <label for="done">Done</label>
        </p>
        <p>
            <label for="deadline">Deadline</label>
            <input type="date" name="deadline" />
        </p>
        <p>
            <button name="delete">Delete</button>
        </p>
        <p>
            <button name="cancel">Cancel</button>
        </p>
    </div>
`

type TodoItem = {
    id: string;
    parent: string | undefined;
    name: string;
    done: boolean;
    level: number;
    deadline: string | undefined;
};

type Mutation = {
    type: "add" | "edit" | "delete";
    item: TodoItem;
    timestamp: number;
};

let unsyncedMutations: Mutation[] = [];

let items: Map<string, TodoItem> = new Map();

type AddItemOptions = {
    id?: string;
    parent?: string;
    name: string;
    done?: boolean;
    deadline?: string;
    sync?: boolean;
};

function addItem(options: AddItemOptions): TodoItem | undefined {
    let id = options.id;
    if(id === undefined || id === "")
        id = crypto.randomUUID();
    let level = 0;
    let parent = options.parent;
    if(parent) {
        let parentItem = items.get(parent);

        // Assume the nodes we add are in topological order
        // And check if the node we are adding is an "orphan" (i.e. its parent was deleted)
        // If so, just don't add it to the items map
        if(!parentItem)
            return;

        level = parentItem.level + 1;
    }
    let name = options.name;
    let done = options.done;
    if(done === undefined)
        done = false;
    let deadline = options.deadline;
    let item = { id, parent, name, level, done, deadline };
    items.set(id, item);
    let sync = options.sync;
    if(sync === undefined)
        sync = true;
    if(sync)
        unsyncedMutations.push({ type: "add", item, timestamp: Date.now() });
    return item;
}

function addItemToGraph(item: TodoItem, graph: Graph) {
    let pos = {x: 0, y: 0};
    let color = "green";
    let labelColor = "black";

    if(item.parent) {
        let r = 1;
        let bearing = Math.random() * 2 * Math.PI;
        let dx = Math.cos(bearing) * r;
        let dy = Math.sin(bearing) * r;

        let px = graph.getNodeAttribute(item.parent, "x");
        let py = graph.getNodeAttribute(item.parent, "y");
        pos = {x: px + dx, y: py + dy};
    }

    if(item.done) {
        color = "gray";
        labelColor = "gray";
    }

    let size = Math.max(50 - item.level * 5, 10);

    graph.addNode(item.id, { label: item.name, x: pos.x, y: pos.y, size, color, labelColor });
    if (item.parent)
        graph.addEdge(item.id, item.parent, { type: "arrow", size: 10 });
}

function highlightChildren(id: string | undefined, graph: Graph) {
    if(id === undefined) {
        for(let item of items.values())
            graph.removeNodeAttribute(item.id, "borderColor");
        return;
    }

    for(let item of items.values()) {
        if(item.id == id || item.parent == id)
            graph.setNodeAttribute(item.id, "borderColor", "#cffafe");
        else
            graph.removeNodeAttribute(item.id, "borderColor");
    }
}

function hasAnyChildren(id: string): boolean {
    for(let item of items.values()) {
        if(item.parent == id)
            return true;
    }
    return false;
}

function setName(id: string, name: string, graph: Graph) {
    let item = items.get(id);
    if(!item)
        return;
    item.name = name;
    unsyncedMutations.push({ type: "edit", item, timestamp: Date.now()});
    if(graph.hasNode(id))
        graph.setNodeAttribute(id, "label", name);
}

function setDone(id: string, done: boolean, graph: Graph) {
    let item = items.get(id);
    if(!item)
        return;
    item.done = done;
    unsyncedMutations.push({ type: "edit", item, timestamp: Date.now()});
    if(graph.hasNode(id)) {
        graph.setNodeAttribute(id, "color", done ? "gray" : "green");
        graph.setNodeAttribute(id, "labelColor", done ? "gray" : "black");
    }
}

function setDeadline(id: string, deadline: string|undefined) {
    let item = items.get(id);
    if(!item)
        return;
    item.deadline = deadline;
    unsyncedMutations.push({ type: "edit", item, timestamp: Date.now()});
}

function deleteItem(id: string, graph: Graph) {
    let item = items.get(id);
    if(!item)
        return;
    items.delete(id);
    unsyncedMutations.push({ type: "delete", item, timestamp: Date.now()});
    graph.dropNode(id);
}

function syncMutations() {
    // TODO: Handle the case where the server is not reachable / we are offline
    let mutationsToPerform: Map<String, Mutation> = new Map();
    for(let mutation of unsyncedMutations) {
        let oldMutation = mutationsToPerform.get(mutation.item.id);
        if(!oldMutation) {
            mutationsToPerform.set(mutation.item.id, mutation);
            continue;
        }
        console.log("Combining mutations", oldMutation, mutation);
        if(oldMutation.type == "add" && mutation.type == "edit") {
            mutationsToPerform.set(mutation.item.id, { type: "add", item: mutation.item, timestamp: mutation.timestamp});
        } else if(oldMutation.type == "edit" && mutation.type == "edit") {
            mutationsToPerform.set(mutation.item.id, mutation);
        } else if(oldMutation.type == "add" && mutation.type == "delete") {
            // Don't do anything
            mutationsToPerform.delete(mutation.item.id);
        } else if(oldMutation.type == "edit" && mutation.type == "delete") {
            mutationsToPerform.set(mutation.item.id, mutation);
        } else {
            console.error("Invalid mutation combination, shoulds never happen");
            return;
        }
    }
    unsyncedMutations = [];
    let mutationPromises = [];
    for(let mutation of mutationsToPerform.values()) {
        if(mutation.timestamp > Date.now() - 500) {
            // This avoids sending edits that are still in-progress as mutations
            unsyncedMutations.push(mutation);
            continue;
        }
        if(mutation.type == "add") {
            let promise = fetch(`${apiRoot}/add-item`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(mutation.item),
            });
            mutationPromises.push(promise);
        } else if(mutation.type == "edit"){
            let promise = fetch(`${apiRoot}/update-item`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(mutation.item),
            });
            mutationPromises.push(promise);
        } else if(mutation.type == "delete") {
            let promise = fetch(`${apiRoot}/delete-item`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(mutation.item),
            });
            mutationPromises.push(promise);
        }
    }

    // Make sure unsyncedMutations are sorted by timestamp
    unsyncedMutations.sort((a, b) => a.timestamp - b.timestamp);

    if(mutationPromises.length > 0) {
        Promise.all(mutationPromises).then(() => {
            console.log("Mutations synced");
        });
    }

    // TODO: Show an indicator that we are syncng
}

setInterval(syncMutations, 5000);

let graph = new Graph();
let apiRoot = "http://localhost:3579";
fetch(`${apiRoot}/items`).then(async (response) => {
    let items = await response.json();
    if(!Array.isArray(items))
        return;
    // TODO: Make sure the items are sorted topologically
    // So that we can add them to the graph properly
    for(let item of items) {
        let deadline: string | undefined = item.deadline;
        if(deadline === "")
            deadline = undefined;
        else
            deadline = deadline!.split("T")[0];
        let newItem = addItem({ id: item.id, 
            parent: item.parent, 
            name: item.name,
            done: item.done,
            deadline: deadline,
            sync: false });
        if(newItem)
            addItemToGraph(newItem, graph);
    }
});

let layout = new ForceSupervisor(graph, { 
    isNodeFixed: (_, attr) => attr.highlighted,
    settings: { attraction: 0.05, repulsion: 0.01, gravity: 0.05 }});
layout.start();

let display = document.getElementsByClassName(styles.display)[0] as HTMLDivElement;

let editor = document.getElementsByClassName(styles.editor)[0] as HTMLDivElement;
let labelInput = editor.querySelector("input[name=label]") as HTMLInputElement;
let doneInput = editor.querySelector("input[name=done]") as HTMLInputElement;
let deadlineInput = editor.querySelector("input[name=deadline]") as HTMLInputElement;
let cancelButton = editor.querySelector("button[name=cancel]") as HTMLButtonElement;
let deleteButton = editor.querySelector("button[name=delete]") as HTMLButtonElement;

let renderer = new Sigma(graph, display, {
    labelColor: { attribute: "labelColor", color: "black" },
    edgeProgramClasses: { arrow: EdgeArrowProgram },

    defaultNodeType: "bordered",
    nodeProgramClasses: { 
        bordered: createNodeBorderProgram({
            borders: [
                // we need to add a default transparent border to avoid the node from being aliased
                { size: { value: 0.1 }, color: { attribute: "borderColor", defaultValue: "#00000000" } },
                { size: { fill: true }, color: { attribute: "color" } }
            ]
        })
    },
});
// State for drag'n'drop
let draggedNode: string | null = null;
let isDragging = false;
let idBeingEdited: string | null = null;

function showEditor(idToEdit: string) {
    editor.style.visibility = "visible";
    editor.style.animationName = styles['enter-stage-right'];
    editor.style.animationDuration = "0.3s";
    editor.style.animationFillMode = "forwards";
    editor.style.animationTimingFunction = "ease-in";

    idBeingEdited = idToEdit;
    let item = items.get(idToEdit);
    if(item === undefined) {
        console.error("Item to edit not found", idToEdit);
        return;
    }
    labelInput.value = item?.name || "";
    doneInput.checked = item?.done || false;
    deadlineInput.value = item?.deadline || "";
}

function hideEditor() {
    editor.style.animationName = styles['exit-stage-right'];
    editor.style.animationDuration = "0.3s";
    editor.style.animationFillMode = "forwards";
    editor.style.animationTimingFunction = "ease-in";

    idBeingEdited = null;
}

// On mouse down on a node
//  - we enable the drag mode
//  - save in the dragged node in the state
//  - highlight the node
//  - disable the camera so its state is not updated
renderer.on("downNode", (e) => {
    isDragging = true;
    draggedNode = e.node;
    graph.setNodeAttribute(draggedNode, "highlighted", true);
});

// On mouse move, if the drag mode is enabled, we change the position of the draggedNode
renderer.getMouseCaptor().on("mousemovebody", (e) => {
    if (!isDragging || !draggedNode)
        return;

    // Get new position of node
    const pos = renderer.viewportToGraph(e);

    graph.setNodeAttribute(draggedNode, "x", pos.x);
    graph.setNodeAttribute(draggedNode, "y", pos.y);

    // Prevent sigma to move camera:
    e.preventSigmaDefault();
    e.original.preventDefault();
    e.original.stopPropagation();
});

// On mouse up, we reset the autoscale and the dragging mode
renderer.getMouseCaptor().on("mouseup", () => {
  if (draggedNode)
      graph.removeNodeAttribute(draggedNode, "highlighted");
  isDragging = false;
  draggedNode = null;
});

// Disable the autoscale at the first down interaction
renderer.getMouseCaptor().on("mousedown", () => {
    if (!renderer.getCustomBBox())
        renderer.setCustomBBox(renderer.getBBox());
});

renderer.on("doubleClickNode", (evt) => {
    let child = addItem({
        parent: evt.node,
        name: "Child",
    });
    if(child)
        addItemToGraph(child, graph);
    evt.preventSigmaDefault();
});

renderer.on("clickNode", (evt) => {
    showEditor(evt.node);
    highlightChildren(evt.node, graph);
});

renderer.on("clickStage", () => {
    hideEditor();
    highlightChildren(undefined, graph);
});

labelInput.addEventListener("input", (e) => {
    if (idBeingEdited == null) 
        return;
    let newLabel = labelInput.value;
    setName(idBeingEdited, newLabel, graph);
});

doneInput.addEventListener("change", (e) => {
    if (idBeingEdited == null) 
        return;
    setDone(idBeingEdited, doneInput.checked, graph);
});

deadlineInput.addEventListener("change", (e) => {
    if (idBeingEdited == null) 
        return;
    let deadline: string | undefined = deadlineInput.value;
    if (deadline === "")
        deadline = undefined;
    setDeadline(idBeingEdited, deadline);
});

window.addEventListener("beforeunload", (e) => {
    if(unsyncedMutations.length > 0)
        e.returnValue = "You have unsynced changes, are you sure you want to leave?";
});

cancelButton.addEventListener("click", () => {
    hideEditor();
});

deleteButton.addEventListener("click", () => {
    if (idBeingEdited == null) 
        return;
    if(hasAnyChildren(idBeingEdited))
        return;
        // TODO: Warn the user that they can't delete a node with children
    let ok = confirm("Are you sure you want to delete this item?");
    if(!ok)
        return;
    deleteItem(idBeingEdited, graph);
    hideEditor();
});
